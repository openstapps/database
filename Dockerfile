FROM elasticsearch:8.4.2

EXPOSE 9200
EXPOSE 9300


ADD config/ /usr/share/elasticsearch/config/

USER root

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu

RUN chown elasticsearch:elasticsearch config/elasticsearch.yml

USER elasticsearch

CMD ["/usr/share/elasticsearch/bin/elasticsearch"]
